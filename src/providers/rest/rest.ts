import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import { mUserinfo } from '../../models/userinfo';
// import { mReward } from '../../models/reward'
// import { threadId } from 'worker_threads';

@Injectable()
export class RestProvider {

  constructor(public http: HttpClient) {
    // console.log('On RestProvider Provider');
  }
  loginTel(telNumber: string): Observable<any> {
    let URL = 'http://47.74.244.86/jlservice/api/chk_Tel';
    let body = { "tel": telNumber };
    return this.http.post<any>(URL, body);
  }
  checkOTP(otpCode: string, telNumber: string): Observable<any> {
    let URL = 'http://47.74.244.86/jlservice/api/chkotpcode';
    let body = {
      "tel": telNumber,
      "otpcode": otpCode
    };
    return this.http.post<any>(URL, body);
  }
  fillProfile(userid: string, name: string, img: string): Observable<any> {
    let URL = 'http://47.74.244.86/jlservice/api/Fill_in_profile';
    let body = {
      "userid": userid,
      "profilename": name,
      "profileimg": img,
    };
    return this.http.post<any>(URL, body);
  }
  fillCoverLeaf(userid: string, invitecode: string): Observable<any> {
    let URL = 'http://47.74.244.86/jlservice/api/fill_cover_leave';
    let body = {
      "userid": userid,
      "invitecode": invitecode
    }
    return this.http.post<any>(URL, body)
  }

  getProfile(userid: string): Observable<mUserinfo> {
    let URL = 'http://47.74.244.86/jlservice/api/getUserInfo/';
    let body = {
      "userid": userid
    };
    return this.http.post<mUserinfo>(URL, body);
  }

  getQuestion(userid: string, roundid: string): Observable<any> {
    let URL = 'http://47.74.244.86/jlservice/api/get_question/';
    let body = {
      "userid": userid,
      "roundid": roundid
    };
    return this.http.post<any>(URL, body);
  }

  chkQuestion(userid: string, roundid: string, id: string, ans_selected: string, leave_amount: string): Observable<any> {
    let URL = 'http://47.74.244.86/jlservice/api/ChkQuestion/';
    let body = {
      "userid": userid,
      "roundid": roundid,
      "id": id,
      "ans_selected": ans_selected,
      "leave_amount": leave_amount
    };
    return this.http.post<any>(URL, body);
  }

  getRewardList(): Observable<any> {
    let URL = 'http://47.74.244.86/jlservice/api/getRewardList'
    let body = {}
    return this.http.post<any>(URL, body)
  }

  getAdList(): Observable<any> {
    let URL = 'http://47.74.244.86/jlservice/api/getAdsList'
    let body = {}
    return this.http.post<any>(URL, body)
  }

  redeem(userid: string, rewardid: string): Observable<any> {
    let URL = 'http://47.74.244.86/jlservice/api/redeem'
    let body = {
      "userid": userid,
      "rewardid": rewardid
    }
    return this.http.post<any>(URL, body)
  }

  redeemHistory(userid): Observable<any> {
    let URL = 'http://47.74.244.86/jlservice/api/getRedeemLog'
    let body = {
      "userid": userid
    }
    return this.http.post<any>(URL, body);
  }

  top10User(): Observable<any> {
    let URL = 'http://47.74.244.86/jlservice/api/getUserTop10'
    let body = {}
    return this.http.post<any>(URL, body)
  }

}
