export interface mUserinfo {
        userid: string;
        score: string;
        leave: string;
        profileimg: string;
        profilename: string;
        AdImg: string;
        AdLink: string;
        flagplay: string;
        roundid: string;
        NextRound: string;
        invitecode: string;
        returncode: string;
        desc: string;
    }