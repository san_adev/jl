import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { StartPage } from '../pages/start/start';
import { LoginPage } from '../pages/login/login';
import { CoverPage } from '../pages/cover/cover';
import { LoginFormPage } from '../pages/login-form/login-form';
import { LoginOtpPage } from '../pages/login-otp/login-otp';
import { ProfilePage } from '../pages/profile/profile';
import { PointPage } from '../pages/point/point';
import { PlayPage } from '../pages/play/play';
import { RestProvider } from '../providers/rest/rest';
import { HttpClientModule } from '@angular/common/http';
import { HTTP } from '@ionic-native/http';
import { NativeStorage } from '@ionic-native/native-storage';
import { IonicStorageModule } from '@ionic/storage';
import { SettingPage } from '../pages/setting/setting';
import { Clipboard } from '@ionic-native/clipboard';
import { ChangeprofilePage } from '../pages/changeprofile/changeprofile';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SelectprofilePage } from '../pages/selectprofile/selectprofile';
import { RulePage } from '../pages/rule/rule';
import { HowtoplayPage } from '../pages/howtoplay/howtoplay';
import { HistoryPage } from '../pages/history/history';
import { InvitePage } from '../pages/invite/invite';
import { PlayresultPage } from '../pages/playresult/playresult';
import { RedeemresultPage } from '../pages/redeemresult/redeemresult';
import { ToptenuserPage } from '../pages/toptenuser/toptenuser';
import { LocalNotifications } from '@ionic-native/local-notifications';


@NgModule({
  declarations: [
    MyApp,
    StartPage,
    LoginPage,
    CoverPage,
    LoginFormPage,
    LoginOtpPage,
    ProfilePage,
    InvitePage,
    PointPage,
    PlayPage,
    HomePage,
    SettingPage,
    ChangeprofilePage,
    SelectprofilePage,
    RulePage,
    HowtoplayPage,
    HistoryPage,
    InvitePage,
    PlayresultPage,
    RedeemresultPage,
    ToptenuserPage,

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    StartPage,
    LoginPage,
    CoverPage,
    LoginFormPage,
    LoginOtpPage,
    ProfilePage,
    InvitePage,
    PointPage,
    PlayPage,
    HomePage,
    SettingPage,
    ChangeprofilePage,
    SelectprofilePage,
    RulePage,
    HowtoplayPage,
    HistoryPage,
    PlayresultPage,
    RedeemresultPage,
    ToptenuserPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    RestProvider,
    HTTP,
    NativeStorage,
    Clipboard,
    InAppBrowser,
    LocalNotifications
  ]
})
export class AppModule { }
