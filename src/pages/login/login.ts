import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController } from 'ionic-angular';
// import { LoginFormPage } from '../login-form/login-form';
// import { NativeStorage } from '@ionic-native/native-storage';
// import { Platform } from 'ionic-angular';
import { LoginFormPage } from '../login-form/login-form';
import { StartPage } from '../start/start';
import {Storage} from '@ionic/storage';
// import {Observable} from 'rxjs/Observable';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  hideMe: boolean = false;
  timer: any;
  maxTime: number;
  hidevalue: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController  ,private storage: Storage) { 
  }
  maxtime: any = 2
  StartTimer(){
    this.timer = setTimeout(x => {
        this.maxtime -= 1;
      
      if (this.maxtime > 0) {
        this.hidevalue = false;
        this.StartTimer();
      }
      else {
        this.hidevalue = true;
        this.storage.keys().then((data) => {
          // console.log(" First : "+data);
          if(Object.keys(data).length === 0){
            this.storage.set('loginStatus', { user: 'none', status: 'logout' })
            .then(
          () => console.log("First JL"),
          error => console.error('Error storing item', error)
        );
          }
          else{
            this.storage.get('loginStatus').then((data) => {
              if(data.user != "none"){
                this.navCtrl.setRoot(StartPage); 
              }else{
                this.navCtrl.push(LoginFormPage);
              }
            }, (error) => {
              console.log(JSON.stringify(error));
            });
          }
        }, (error) => { console.log(JSON.stringify(error)); });

      }
    }, 1000);
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'ตรวจสอบการเข้าระบบ'
    });
  
    loading.present();
  
    setTimeout(() => {
      loading.dismiss();
    }, 3000);
  }

  ionViewDidEnter() {
    this.StartTimer();
    this.presentLoadingDefault(); 
  }
  ionViewDidLoad() {
    // this.storage.clear();
  }

  gotoLogin() {
    this.navCtrl.push(LoginFormPage);
  }
}
