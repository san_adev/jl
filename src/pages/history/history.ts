import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest'

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  redeemHistoryList: any

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public rest: RestProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
    this.storage.get('userId').then((data) => {
      this.rest.redeemHistory(data.userid).subscribe(result => {
        console.log('redeem history', result);
        this.redeemHistoryList = result
      })
    })
  }

}
