import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { CoverPage } from '../cover/cover';
import { LoginPage } from '../login/login';
import { PlayPage } from '../play/play';
import { PointPage } from '../point/point';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(public navCtrl: NavController, private alertCtrl: AlertController) {
  }
  logout() {
    this.presentConfirmLogout();
  }
  presentConfirmLogout() {
    let alert = this.alertCtrl.create({
      title: 'Confirm logout?',
      // message: 'You want to  exit JL?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Logout',
          handler: () => {
            this.navCtrl.push(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }
  gotoCover() {
    this.navCtrl.push(CoverPage);
  }
  playGame() {
    this.navCtrl.setRoot(PlayPage);
  }
  Point() {
    this.navCtrl.setRoot(PointPage);
  }
}
