import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest'
import { HTTP } from '@ionic-native/http';
import { InvitePage } from '../invite/invite'
import { SelectprofilePage } from '../selectprofile/selectprofile';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public restProvider: RestProvider, public http: HTTP, private storage: Storage) {
    this.showProfile()
  }

  @ViewChild("name") fName;
  // @ViewChild("invite") fInvite;

  imgUrl: string;
  iduser: string;

  showProfile() {
    this.storage.get('imgselect').then((item) => {
      if (item.img == "notset") {
        this.imgUrl = 'assets/imgs/profile/3.png'
      } else {
        this.imgUrl = item.img;
      }
    });

    this.storage.get('userId').then((item) => {
      this.iduser = item.userid;
    });
  }

  nextStep() {
    this.navCtrl.setRoot(InvitePage);
  }

  next() {

    let userid = this.iduser;
    let name = this.fName.value;
    let img = this.imgUrl;
    // let invite = this.fInvite.value;

    // alert(userid);
    this.restProvider.fillProfile(userid, name, img).subscribe(result => {
      console.log(result);

      if (result.returncode == "1000") {

        let loading = this.loadingCtrl.create({
          content: 'กำลังบันทึกข้อมูล'
        });

        this.storage.set('userName', { userName: name })

        loading.present();

        setTimeout(() => {
          loading.dismiss();
          this.nextStep();
        }, 2500);
      }
    });
  }

  changeProfile() {
    this.navCtrl.push(SelectprofilePage);
  }


}
