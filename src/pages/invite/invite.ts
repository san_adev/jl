import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest'
import { HTTP } from '@ionic-native/http';
import { StartPage } from '../start/start';
import { SelectprofilePage } from '../selectprofile/selectprofile';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the InvitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invite',
  templateUrl: 'invite.html',
})
export class InvitePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public restProvider: RestProvider, public http: HTTP, private storage: Storage) {
    this.showProfile()
  }

  @ViewChild("invite") fInvite;

  imgUrl: string;
  iduser: string;
  nameuser: string;

  showProfile() {
    this.storage.get('imgselect').then((item) => {
      if (item.img == "notset") {
        this.imgUrl = 'assets/imgs/profile/3.png'
      } else {
        this.imgUrl = item.img;
      }
    });

    this.storage.get('userId').then((item) => {
      this.iduser = item.userid;
    });
  }

  nextStep() {
    this.navCtrl.setRoot(StartPage);
  }

  next() {

    let userid = this.iduser;
    let invite = this.fInvite.value;
    
    if (invite == '') {
      setTimeout(() => {
        this.nextStep();
      }, 2500);
    } else {
      this.restProvider.fillCoverLeaf(userid, invite).subscribe(result => {
        console.log(result);

        if (result.returncode == "1000") {

          let loading = this.loadingCtrl.create({
            content: 'กำลังบันทึกข้อมูล'
          });

          loading.present();

          setTimeout(() => {
            loading.dismiss();
            this.nextStep();
          }, 2500);
        }
      });
    }
    // alert(userid);

  }

  changeProfile() {
    this.navCtrl.push(SelectprofilePage);
  }

}
