import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
// import { StartPage } from '../start/start';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';
import { RedeemresultPage } from '../redeemresult/redeemresult'


@IonicPage()
@Component({
  selector: 'page-point',
  templateUrl: 'point.html',
})
export class PointPage {

  rewardList: any
  userid: string;
  userscore: string;
  profileimg: string;
  profilename: string;
  resultMessage: string

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,
    public loadingCtrl: LoadingController, public restProvider: RestProvider, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('point page did load');

    this.storage.get('profile').then((data) => {
      this.userid = data.userid
      this.profilename = data.profilename
      this.profileimg = data.profileimg
      this.userscore = data.score
      this.restProvider.getRewardList().subscribe(result => {
        // console.log('reward list',result);
        this.rewardList = result
        console.log(this.rewardList);

      })
      console.log('user score', this.userscore);
    }, error => console.error('Error get storing item', error))
  }

  reedeem(rewardId) {
    console.log('reddem reward id', rewardId);
    let loading = this.loadingCtrl.create({
      content: 'กำลังตรวจสอบข้อมูล'
    });
    loading.present();
    this.restProvider.redeem(this.userid, rewardId).subscribe(result => {

      console.log('redeem result', result);
      this.resultMessage = result.messege
      console.log('result message', this.resultMessage);
      this.storage.set('redeemResult', { message: this.resultMessage }).then(
        () => console.log("save redeem message"),
        error => console.error('Error storing item', error)
      );
      setTimeout(() => {
        loading.dismiss();
        // console.log('in set timeout message', result.message);

        this.redeemResultPage()
      }, 2500);
    })

  }

  redeemResultPage() {
    this.navCtrl.push(RedeemresultPage)
  }

}
