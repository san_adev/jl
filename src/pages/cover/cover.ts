import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { StartPage } from '../start/start';
import { Clipboard } from '@ionic-native/clipboard';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-cover',
  templateUrl: 'cover.html',
})
export class CoverPage {

  inviteCode: string
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    private alertCtrl: AlertController, private clipboard: Clipboard, private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CoverPage');
    this.storage.get('profile').then((data) => {

      this.inviteCode = data.inviteCode
      console.log('user invite code', this.inviteCode);
    }, error => console.error('Error get storing item', error))

  }

  goHome() {
    this.navCtrl.setRoot(StartPage);
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'กำลังตรวจสอบ'
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
      this.goHome();
      this.presentAlert();
    }, 2500);
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'ทำรายการสำเร็จ',
      subTitle: 'ตัวช่วยเพิ่ม x 5',
      buttons: ['Close']
    });
    alert.present();
  }

  copy() {
    this.clipboard.copy(this.inviteCode);
    // 
    let alert = this.alertCtrl.create({
      title: 'คัดลอกสำเร็จ'
    })
    alert.present()
    setTimeout(() => {
      alert.dismiss()
    }, 1000)
  }
}
