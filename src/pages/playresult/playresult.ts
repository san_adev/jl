import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { StartPage } from '../start/start'

@IonicPage()
@Component({
  selector: 'page-playresult',
  templateUrl: 'playresult.html',
})
export class PlayresultPage {

  totalScore: string
  showMessage: string
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
  }

  ionViewDidLoad() {
    this.storage.get('playResult').then((data)=> {
      this.totalScore = data.score
      this.showMessage = data.message
    })
  }

  gotoStart() {
    this.navCtrl.setRoot(StartPage)
  }

}
