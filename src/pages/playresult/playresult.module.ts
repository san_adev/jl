import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlayresultPage } from './playresult';

@NgModule({
  declarations: [
    PlayresultPage,
  ],
  imports: [
    IonicPageModule.forChild(PlayresultPage),
  ],
})
export class PlayresultPageModule {}
