import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest'
import { HTTP } from '@ionic-native/http';
// import { StartPage } from '../start/start';
import { LoginOtpPage } from '../login-otp/login-otp';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { ProfilePage } from '../profile/profile';
import { Storage } from '@ionic/storage';
import { StartPage } from '../start/start';

@IonicPage()
@Component({
  selector: 'page-login-form',
  templateUrl: 'login-form.html',
})
export class LoginFormPage {

  // , public http: HttpClient
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public restProvider: RestProvider, public http: HTTP,
    private localnotifications: LocalNotifications, private storage: Storage) {
  }

  hideMe: boolean = false;
  timer: number;
  maxTime: number;
  hidevalue: boolean;


  // maxtime: any = 2
  // StartTimer() {
  //   this.timer = setTimeout(x => {
  //     this.maxtime -= 1;
  //     console.log(this.maxtime);

  //     if (this.maxtime > 0) {
  //       this.hidevalue = false;
  //       this.StartTimer();
  //     }
  //     else {
  //       this.hidevalue = true;

  //     }
  //   }, 1000);
  // }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'ระบบกำลังส่งรหัส OTP'
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 3000);
  }

  noti() {
    this.localnotifications.schedule({
      id: 1,
      title: 'JL Notification',
      text: 'รอบเวลา 8.30 ใกล้เข้ามาแล้ว อย่าลืมมาร่วมสนุกกันนะครับ',
      // trigger: {  },
      led: 'FF0000',
      every: { hour: 8, minute: 20 }
    });
    this.localnotifications.schedule({
      id: 2,
      title: 'JL Notification',
      text: 'รอบเวลา 11.30 ใกล้เข้ามาแล้ว อย่าลืมมาร่วมสนุกกันนะครับ',
      // trigger: {  },
      led: 'FF0000',
      every: { hour: 11, minute: 20 }
    });
    this.localnotifications.schedule({
      id: 3,
      title: 'JL Notification',
      text: 'รอบเวลา 14.30 ใกล้เข้ามาแล้ว อย่าลืมมาร่วมสนุกกันนะครับ',
      // trigger: {  },
      led: 'FF0000',
      every: { hour: 14, minute: 20 }
    });
    this.localnotifications.schedule({
      id: 4,
      title: 'JL Notification',
      text: 'รอบเวลา 17.30 ใกล้เข้ามาแล้ว อย่าลืมมาร่วมสนุกกันนะครับ',
      // trigger: {  },
      led: 'FF0000',
      every: { hour: 17, minute: 20 }
    });
    this.localnotifications.schedule({
      id: 5,
      title: 'JL Notification',
      text: 'รอบเวลา 20.30 ใกล้เข้ามาแล้ว อย่าลืมมาร่วมสนุกกันนะครับ',
      // trigger: {  },
      led: 'FF0000',
      every: { hour: 20, minute: 20 }
    });
  }

  @ViewChild("telNumber") mTelNumber;

  ionViewDidLoad() {
  }

  ionViewDidEnter() {
    this.noti();

  }

  next() {
    let rTel = this.mTelNumber.value;
    this.restProvider.loginTel(rTel).subscribe(result => {
      console.log(result);
      if (result.returncode == "1000") {
        // this.StartTimer();
        if (rTel != "0901924640") {
          this.presentLoadingDefault();
          this.navCtrl.push(LoginOtpPage, {
            telNumber: this.mTelNumber.value
          });
        }
        else {
          this.storage.set('userId', { userid: result.userid, tel: result.tel })
          this.navCtrl.setRoot(StartPage);

        }

      }
    });
  }

}
