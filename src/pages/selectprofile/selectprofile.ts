import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ProfilePage } from '../profile/profile';

@IonicPage()
@Component({
  selector: 'page-selectprofile',
  templateUrl: 'selectprofile.html',
})
export class SelectprofilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
    this.storage.set('imgselect', { img: " " });
  }

  changeUrl(url) {
    this.storage.set('imgselect', { img: url });
  }

  ok() {
    this.navCtrl.setRoot(ProfilePage);
  }

}
