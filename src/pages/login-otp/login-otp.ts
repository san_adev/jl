import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { RestProvider } from '../../providers/rest/rest'
import { HTTP } from '@ionic-native/http';
import { StartPage } from '../start/start';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-login-otp',
  templateUrl: 'login-otp.html',
})
export class LoginOtpPage {

  @ViewChild("otpCode") otpCode;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public restProvider: RestProvider, public http: HTTP, private alertCtrl: AlertController, private storage: Storage, ) {
    this.storage.set('imgselect', { img: "notset" })
  }

  ionViewDidLoad() {}

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'เข้าสู่ระบบผิดพลาด',
      message: 'รหัสยืนยันไม่ถูกต้อง',
      buttons: [
        {
          text: 'กลับ',
          role: 'cancel',
          handler: () => {
            this.navCtrl.pop();
          }
        },
        {
          text: 'ขอรหัสอีกครั้ง',
          handler: () => {
            let rTel = "";
            rTel = this.navParams.get("telNumber")

            this.restProvider.loginTel(rTel).subscribe(result => {
              console.log(result.returncode);
            });
          }
        }
      ]
    });
    alert.present();
  }

  nextStep() {
    let tel = "";
    tel = this.navParams.get("telNumber")

    let otp = this.otpCode.value;
    this.restProvider.checkOTP(otp, tel).subscribe(result => {
      // console.log(result);
      let userid = result.desc;
      if (result.returncode == "3800") {
        this.presentConfirm();
        // this.navCtrl.setRoot(StartPage);
      } else if (result.returncode == "1000") {
        this.storage.set('userId', { userid: userid , tel: tel })
        this.navCtrl.setRoot(ProfilePage, {
          userID: userid
        });
      } else if (result.returncode == "1100") {
        this.storage.set('userId', { userid: userid , tel: tel })
        this.navCtrl.setRoot(StartPage);
      }
      // this.navCtrl.push(LoginOtpPage);
    });
  }

  next() {
    let loading = this.loadingCtrl.create({
      content: 'กำลังตรวจสอบรหัส OTP'
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
      this.nextStep();
    }, 2500);
  }

}
