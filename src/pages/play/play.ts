import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { StartPage } from '../start/start';
import { RestProvider } from '../../providers/rest/rest'
import { HTTP } from '@ionic-native/http';
import { Storage } from '@ionic/storage';
import { PlayresultPage } from '../playresult/playresult'

@IonicPage()
@Component({
  selector: 'page-play',
  templateUrl: 'play.html',
})
export class PlayPage {
  timer: any;
  picCover1 = "assets/imgs/cover.png";
  picCover2 = "assets/imgs/cover.png";
  picCover3 = "assets/imgs/cover.png";
  maxtime: any = 10;

  // ansCorrect: boolean

  roundid: any;

  questionid: string;
  question: string;
  ans: string;
  c1: string;
  c2: string;
  c3: string;
  c4: string;
  point: string;
  choice: string;
  leave: string;

  stopGame: boolean = false

  isReadonly: boolean = false

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider: RestProvider, public http: HTTP, private storage: Storage,
    public alertCtrl: AlertController) {

    this.roundid = navParams.get('roundid');

  }
  ionViewDidEnter() {
    this.getQuestion()
  }

  getQuestion() {

    this.storage.get('profile').then((data) => {
      this.restProvider.getQuestion(data.userid, this.roundid).subscribe(result => {
        console.log('first question:' + result + "   sss " + this.roundid);

        this.questionid = result.id
        this.question = result.Question
        this.c1 = result.C1
        this.c2 = result.C2
        this.c3 = result.C3
        this.c4 = result.C4
        if (result.FlagPromo == "y") {
          this.point = "ข้อนี้ " + result.Point + " คะแนน (x2)";
        }
        else {
          this.point = "ข้อนี้ " + result.Point + " คะแนน";
        }
        this.ans = result.ans
        this.leave = result.leave

        this.showCover(this.leave);

        this.startGame();
      });
    }, (error) => {
      console.log(JSON.stringify(error));
    });
  }

  checkAns(select: string) {

    this.isReadonly = true;

    if (select == "c1") { this.choice = this.c1 }
    if (select == "c2") { this.choice = this.c2 }
    if (select == "c3") { this.choice = this.c3 }
    if (select == "c4") { this.choice = this.c4 }


    console.log('question answer', this.ans)
    console.log('select choice', this.choice);
    console.log('user answer is correct?', this.choice == this.ans);

    this.storage.get('profile').then((data) => {


      clearTimeout(this.timer)

      this.restProvider.chkQuestion(data.userid, this.roundid, this.questionid, this.choice, this.leave)
      .subscribe(result => {
        console.log('check questitont:', result);

        if (this.choice == this.ans && result.FlagStop != 'y') {


          console.log('test alert');

          let alert = this.alertCtrl.create({
            cssClass: 'correct-alert'
          })
          alert.present()
          setTimeout(() => {
            console.log('diss miss alert');
            alert.dismiss()
          }, 1000)


        } else if (this.choice != this.ans && result.FlagStop != 'y') {

          console.log('test alert');

          let alert = this.alertCtrl.create({
            cssClass: 'incorrect-alert'
          })
          alert.present()
          setTimeout(() => {
            console.log('diss miss alert');

            alert.dismiss()
          }, 1000)


        }

        console.log('bind data');
        console.log('show new question');

        this.questionid = result.id
        this.question = result.Question
        this.c1 = result.C1
        this.c2 = result.C2
        this.c3 = result.C3
        this.c4 = result.C4
        if (result.FlagPromo == "y") {
          this.point = "ข้อนี้ " + result.Point + " คะแนน (x2)";
        }
        else {
          this.point = "ข้อนี้ " + result.Point + " คะแนน";
        }
        this.ans = result.ans
        this.leave = result.leave
        this.showCover(this.leave);
        this.choice = '';

        this.isReadonly = false;

        if (result.FlagStop == 'y') {
          if (result.FlagWin == 'y') {

            console.log(result.real_correct);
            if (result.real_correct == "y") {
              console.log('test alert');

              let alert = this.alertCtrl.create({
                cssClass: 'correct-alert'
              })
              alert.present()
              setTimeout(() => {
                console.log('diss miss alert');
                alert.dismiss()
              }, 1000)
            } else if (result.real_correct == "n") {
              console.log('test alert');

              let alert = this.alertCtrl.create({
                cssClass: 'incorrect-alert'
              })
              alert.present()
              setTimeout(() => {
                console.log('diss miss alert');

                alert.dismiss()
              }, 1000)
            }

            console.log(result.TotalScore);
            this.storage.set('playResult', { score: result.TotalScore, message: 'ยินดีด้วยค่ะคุณคือผู้ชนะ' }).then(
              () => console.log("save redeem message"),
              error => console.error('Error storing item', error)
            );
            this.navCtrl.setRoot(PlayresultPage)
            // this.navCtrl.setRoot(StartPage);
          } else {

            let alert = this.alertCtrl.create({
              cssClass: 'incorrect-alert'
            })
            alert.present()
            setTimeout(() => {
              console.log('diss miss alert');

              alert.dismiss()
            }, 1000)

            console.log(result.TotalScore);
            this.storage.set('playResult', { score: result.TotalScore, message: 'มาเล่นกันใหม่ ในรอบถัดไปนะคะ' }).then(
              () => console.log("save redeem message"),
              error => console.error('Error storing item', error)
            );
            this.navCtrl.setRoot(PlayresultPage)
          }
        }
        this.startGame();
        // }, 1300)
      });
    });

  }

  ansClick() {
    // this.goHome();
    this.maxtime = 0;
  }

  showCover(cover) {
    if (cover == 3) {
      this.picCover1 = "assets/imgs/cover.png";
      this.picCover2 = "assets/imgs/cover.png";
      this.picCover3 = "assets/imgs/cover.png";
    } else if (cover == 2) {
      this.picCover1 = "assets/imgs/coverGray.png";
      this.picCover2 = "assets/imgs/cover.png";
      this.picCover3 = "assets/imgs/cover.png";
    } else if (cover == 1) {
      this.picCover1 = "assets/imgs/coverGray.png";
      this.picCover2 = "assets/imgs/coverGray.png";
      this.picCover3 = "assets/imgs/cover.png";
    } else if (cover == 0) {
      this.picCover1 = "assets/imgs/coverGray.png";
      this.picCover2 = "assets/imgs/coverGray.png";
      this.picCover3 = "assets/imgs/coverGray.png";
    }
  }

  goHome() {
    this.navCtrl.setRoot(StartPage);
  }

  startGame() {
    this.maxtime = 11;
    this.StartTimer();
  }

  StartTimer() {
    this.timer = setTimeout(x => {
      this.maxtime -= 1;
      // console.log(this.maxtime);
      if (this.maxtime > 0) {
        this.StartTimer();
      } else {
        this.storage.get('profile').then((data) => {

          clearTimeout(this.timer)

          console.log("stop Auto")
          console.log('user not answer local data', data);
          console.log('choice', this.choice);

          if ((this.choice == undefined || this.choice == '')) {

            this.choice = ''
            console.log('no answer choice', this.choice);

            this.restProvider.chkQuestion(data.userid, this.roundid, this.questionid, this.choice,
              this.leave).subscribe(result => {
                console.log('user not answer', result);

                if (result.Question != '') {


                  if (result.FlagStop != 'y') {
                    let alt = this.alertCtrl.create({
                      cssClass: 'incorrect-alert'
                    })
                    alt.present()
                    setTimeout(() => {
                      console.log('diss miss alert');
                      alt.dismiss()
                    }, 1000)
                  }


                  this.questionid = result.id
                  this.question = result.Question
                  this.c1 = result.C1
                  this.c2 = result.C2
                  this.c3 = result.C3
                  this.c4 = result.C4
                  if (result.FlagPromo == "y") {
                    this.point = "ข้อนี้ " + result.Point + " คะแนน (x2)";
                  }
                  else {
                    this.point = "ข้อนี้ " + result.Point + " คะแนน";
                  }
                  this.ans = result.ans
                  this.leave = result.leave
                  this.showCover(this.leave);

                  this.startGame();



                } else {
                  if (result.FlagStop == 'y') {
                    
                    console.log(result.TotalScore);

                    if (result.FlagWin == 'y') {


                      let alert = this.alertCtrl.create({
                        cssClass: 'incorrect-alert'
                      })
                      alert.present()
                      setTimeout(() => {
                        console.log('diss miss alert');
                        alert.dismiss()
                      }, 1000)


                      this.storage.set('playResult', { score: result.TotalScore, message: 'ยินดีด้วยค่ะคุณคือผู้ชนะ' }).then(
                        () => console.log("save redeem message"),
                        error => console.error('Error storing item', error)
                      );
                      this.navCtrl.setRoot(PlayresultPage)
                    } else {


                      let alert = this.alertCtrl.create({
                        cssClass: 'incorrect-alert'
                      })
                      alert.present()
                      setTimeout(() => {
                        console.log('diss miss alert');

                        alert.dismiss()
                      }, 1000)




                      this.storage.set('playResult', { score: result.TotalScore, message: 'มาเล่นกันใหม่ ในรอบถัดไปนะคะ' }).then(
                        () => console.log("save redeem message"),
                        error => console.error('Error storing item', error)
                      );
                      this.navCtrl.setRoot(PlayresultPage)
                    }

                  }

                }
              })

          } else {
            this.startGame();
          }








        })
      }
    }, 1300);
  }

  endGame() {
    this.navCtrl.setRoot(StartPage);
  }
}