import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Slides } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { CoverPage } from '../cover/cover';
// import { LoginPage } from '../login/login';
import { PlayPage } from '../play/play';
import { PointPage } from '../point/point';
import { Storage } from '@ionic/storage';
import { SettingPage } from '../setting/setting';
import { RestProvider } from '../../providers/rest/rest'
import { HTTP } from '@ionic-native/http';
import { mUserinfo } from '../../models/userinfo';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { every } from '../../../node_modules/rxjs/operators';

@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})

export class StartPage {
  
  @ViewChild(Slides) slides: Slides;
  alarm: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,
    public showAlert: AlertController,public restProvider: RestProvider, public http: HTTP, private iab: InAppBrowser, public platform: Platform
    ,private localnotifications: LocalNotifications) {
    this.width = '100%';
    platform.ready().then((readySource) => {
      this.height = (platform.height() * 70) / 100;
      this.height = platform.height() - this.height;
      this.height = this.height + 'px';
    });
    this.saveStatusLogin();  
    
    // this.platform.ready().then(()=>{
    // this.localnotifications.on('click').subscribe(res =>{
    //   console.log('click: ' , res );
    //   let msg = res.data ? res.data.myData : '';
    //   this.showAlert(res.title , res.text , msg);
    // });
    // this.localnotifications.on('trigger').subscribe(res =>{      //   console.log('trigger: ' , res );
    //   this.noti();
    // let msg = res.data ? res.data.myData : '';
    // this.showAlert(res.title , res.text , msg);
    //   });
    // });
    
  }

  height: any;
  width: any;

  infoUser: mUserinfo;

  NextRound: string;
  roundid: string;
  flagplay: string;

  userid: string;
  leave: string;
  score: string;
  profileimg: string;
  profilename: string;
  tel: string;

  AdImg: string;
  AdLink: string;

  inviteCode: string;

  AdList: any

 

  ionViewDidEnter() {
    // console.log('startpage did enter');
    this.storage.get('userId').then((data) => {
      this.restProvider.getProfile(data.userid).subscribe(result => {
        console.log('get profile result', result);
        this.infoUser = result;

        if (this.infoUser.NextRound == "") {
          this.NextRound = "8.30"
        }
        else {
          this.NextRound = this.infoUser.NextRound;

        }

        this.roundid = this.infoUser.roundid;
        this.flagplay = this.infoUser.flagplay;

        this.userid = data.userid;
        this.leave = this.infoUser.leave;
        this.score = this.infoUser.score;
        this.profileimg = this.infoUser.profileimg;
        this.profilename = this.infoUser.profilename;
        this.tel = data.tel;

        this.inviteCode = this.infoUser.invitecode

        this.storage.set('profile', {
          profilename: this.profilename, score: this.score, leave: this.leave, profileimg: this.profileimg, tel: this.tel, userid: this.userid,
          NextRound: this.NextRound, roundid: this.roundid, flagplay: this.flagplay,
          AdImg: this.AdImg, AdLink: this.AdLink, inviteCode: this.inviteCode
        }).then(
          () => console.log("save Profile"),
          error => console.error('Error storing item', error)
        );

        this.restProvider.getAdList().subscribe(result => {
          this.AdList = result
          setTimeout(() => {
            // console.log(this.slides);
            this.slides.autoplay = 1000
            this.slides.startAutoplay()
            // this.slides.autoplayDisableOnInteraction=false
            // this.slides.startAutoplay()
          }, 500)
        })
      });
    }, (error) => {
      console.log(JSON.stringify(error));
    });
  }


  saveStatusLogin() {
    this.storage.get('userId').then((data) => {
      this.storage.set('loginStatus', { user: data.tel, status: 'login' })
    });
  }

  goSetting() {
    this.slides.stopAutoplay()
    this.navCtrl.push(SettingPage);
  }

  adbLink(adLink) {
    this.iab.create(adLink, "_system");
  }

  addCover() {
    this.slides.stopAutoplay()
    this.navCtrl.push(CoverPage);
  }

  playGame() {
    this.slides.stopAutoplay()
    this.navCtrl.setRoot(PlayPage, { 'roundid': this.roundid });
  }

  Point() {
    this.slides.stopAutoplay()
    this.navCtrl.push(PointPage);

  }

  refresh_profile() {
    this.storage.get('userId').then((data) => {
      this.restProvider.getProfile(data.userid).subscribe(result => {
        console.log('get profile result', result);
        this.infoUser = result;

        if (this.infoUser.NextRound == "") {
          this.NextRound = "8.30"
        }
        else {
          this.NextRound = this.infoUser.NextRound;

        }
        this.roundid = this.infoUser.roundid;
        this.flagplay = this.infoUser.flagplay;

        this.userid = data.userid;
        this.leave = this.infoUser.leave;
        this.score = this.infoUser.score;
        this.profileimg = this.infoUser.profileimg;
        this.profilename = this.infoUser.profilename;
        this.tel = data.tel;

        this.inviteCode = this.infoUser.invitecode;

      });
    }, (error) => {
      console.log(JSON.stringify(error));
    });
  }




}
