import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest'
import { HTTP } from '@ionic-native/http';
import { Storage } from '@ionic/storage';
import { StartPage } from '../start/start';

/**
 * Generated class for the ChangeprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changeprofile',
  templateUrl: 'changeprofile.html',
})
export class ChangeprofilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider: RestProvider, public http: HTTP, public loadingCtrl: LoadingController, private storage: Storage) {
  }

  profileUrl: string;
  nameProfile: string;
  @ViewChild("name") fName;

  ionViewDidLoad() {
    this.storage.get('profile').then((data) => { 
      this.nameProfile = data.profilename;
    }); 
  }

  changeUrl(url) {
    this.profileUrl = url;
  }
  
  changeOk() {
    this.storage.get('userId').then((data) => {
        this.restProvider.fillProfile(data.userid, this.fName.value , this.profileUrl).subscribe(result => {
          console.log(result);
          if (result.returncode == "1000") {
            let loading = this.loadingCtrl.create({
              content: 'กำลังบันทึกข้อมูล'
            });
            loading.present();
            setTimeout(() => {
              loading.dismiss();
              this.navCtrl.setRoot(StartPage);
            }, 2500);
          }
        });
    });
  }

}
