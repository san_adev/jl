import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { CoverPage } from '../cover/cover';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';
import { ChangeprofilePage } from '../changeprofile/changeprofile';
import { HistoryPage } from '../history/history';
import { HowtoplayPage } from '../howtoplay/howtoplay';
import { RulePage } from '../rule/rule';
import { ToptenuserPage } from '../toptenuser/toptenuser'

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  name: string;
  score: string;
  leave: string;
  profileimg: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
    this.getProfile();
  }

  getProfile() {
    this.storage.get('profile').then((data) => {
      this.name = data.profilename;
      this.score = data.score;
      this.leave = data.leave;
      this.profileimg = data.profileimg;
    }, (error) => {
      console.log(JSON.stringify(error));
    });
  }

  presentConfirmLogout() {
    let alert = this.alertCtrl.create({
      title: 'Confirm logout?',
      // message: 'You want to  exit JL?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Logout',
          handler: () => {
            this.storage.set('loginStatus', { user: 'none', status: 'logout' })
              .then(
                () => console.log("Login JL"),
                error => console.error('Error storing item', error)
              );

            this.navCtrl.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }

  menuTopTen() {    
    this.navCtrl.push(ToptenuserPage);
  }

  menuHowtoplay() {
    this.navCtrl.push(HowtoplayPage);
  }

  menuHistory() {
    this.navCtrl.push(HistoryPage);
  }

  menuFriend() {
    this.navCtrl.push(CoverPage);
  }

  menuService() {
    this.navCtrl.push(RulePage);
  }

  changeProfile() {
    this.navCtrl.push(ChangeprofilePage);
  }



}
