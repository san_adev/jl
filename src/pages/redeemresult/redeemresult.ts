import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { StartPage } from '../start/start'
/**
 * Generated class for the RedeemresultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-redeemresult',
  templateUrl: 'redeemresult.html',
})
export class RedeemresultPage {

  resultMessage: any

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad RedeemresultPage');
    this.storage.get('redeemResult').then((data) => {
      console.log(data);

      this.resultMessage = data.message
      console.log('redeem result', this.resultMessage);
    })
  }

  gotoStart() {
    this.navCtrl.setRoot(StartPage)
  }

}
