import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RedeemresultPage } from './redeemresult';

@NgModule({
  declarations: [
    RedeemresultPage,
  ],
  imports: [
    IonicPageModule.forChild(RedeemresultPage),
  ],
})
export class RedeemresultPageModule {}
