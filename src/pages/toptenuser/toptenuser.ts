import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest'

@IonicPage()
@Component({
  selector: 'page-toptenuser',
  templateUrl: 'toptenuser.html',
})
export class ToptenuserPage {
  
  topUserList: any

  constructor(public navCtrl: NavController, public navParams: NavParams, public rest: RestProvider) {
  }

  ionViewDidLoad() {
    this.rest.top10User().subscribe( result => {
      this.topUserList = result
    })
  }

}
