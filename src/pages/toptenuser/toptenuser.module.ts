import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ToptenuserPage } from './toptenuser';

@NgModule({
  declarations: [
    ToptenuserPage,
  ],
  imports: [
    IonicPageModule.forChild(ToptenuserPage),
  ],
})
export class ToptenuserPageModule {}
